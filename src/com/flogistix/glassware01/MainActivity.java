package com.flogistix.glassware01;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.view.View;

import com.google.android.glass.app.Card;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		ArrayList<String> voiceResults = getIntent().getExtras()
		        .getStringArrayList(RecognizerIntent.EXTRA_RESULTS);
		
		StringBuilder builder = new StringBuilder();
		
		for (String value : voiceResults) {
			builder.append(value);
		}
		
//		String[] words = builder.toString().split("\\s");
		
		
		
//		Integer number = Integer.parseInt(builder.toString());
		
		Card card1 = new Card(this);
		card1.setText(builder.toString());
		card1.setInfo(voiceResults.toString());
//		for (Integer i = 0; i < number; i++)
//			card1.addImage(R.drawable.icp);
		
		View card1View = card1.toView();
		
		setContentView(card1View);
	}

}

